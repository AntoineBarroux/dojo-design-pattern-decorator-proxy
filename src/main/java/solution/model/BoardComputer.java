package solution.model;

public class BoardComputer extends AbstractCarDecorator {

    private static final Integer BOARD_COMPUTER_PRICE = 1500;

    public BoardComputer(final Car delegate) {
        super(delegate);
    }

    @Override
    public String getName() {
        return delegate.getName() + " - with board computer";
    }

    @Override
    public Integer getPrice() {
        return delegate.getPrice() + BOARD_COMPUTER_PRICE;
    }
}
