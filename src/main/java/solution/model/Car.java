package solution.model;

public abstract class Car {

    protected String name;
    protected Integer price;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(final Integer price) {
        this.price = price;
    }

    public String toString() {
        return "Building a " + getName() + " that costs " + getPrice() + "€";
    }
}
