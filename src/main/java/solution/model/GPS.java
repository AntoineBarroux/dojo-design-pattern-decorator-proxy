package solution.model;

public class GPS extends AbstractCarDecorator {

    private static final Integer GPS_PRICE = 1000;

    public GPS(final Car delegate) {
        super(delegate);
    }

    @Override
    public Integer getPrice() {
        return delegate.getPrice() + GPS_PRICE;
    }

    @Override
    public String getName() {
        return delegate.getName() + " - with GPS";
    }
}
