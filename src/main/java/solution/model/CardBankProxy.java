package solution.model;

public class CardBankProxy implements Bank {
    private final BankAccount bankAccount = new BankAccount(10000, 1234);
    private boolean validPin;

    public boolean openAccessToAccount(int pin) {
        if(pin == bankAccount.getPin()) {
            validPin = true;
        } else {
            validPin = false;
        }
        return validPin;
    }

    @Override
    public double checkAmount() throws IllegalAccessException {
        if(validPin) {
            return bankAccount.checkAmount();
        }
        throw new IllegalAccessException("Not authenticated");
    }

    @Override
    public double payOrWithdrawAmount(double amount) throws IllegalAccessException {
        if(validPin) {
            return bankAccount.payOrWithdrawAmount(amount);
        }
        throw new IllegalAccessException("Not authenticated");
    }


}
