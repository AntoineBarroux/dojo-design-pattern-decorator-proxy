package solution.model;

public class SunRoof extends AbstractCarDecorator {

    private static final Integer SUN_ROOF_PRICE = 3000;

    public SunRoof(final Car delegate) {
        super(delegate);
    }

    @Override
    public String getName() {
        return delegate.getName() + " - with sunroof";
    }

    @Override
    public Integer getPrice() {
        return delegate.getPrice() + SUN_ROOF_PRICE;
    }
}
