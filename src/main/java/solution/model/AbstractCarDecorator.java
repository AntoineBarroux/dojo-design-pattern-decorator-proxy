package solution.model;

public abstract class AbstractCarDecorator extends Car {

    protected Car delegate;

    public AbstractCarDecorator(Car delegate) {
        this.delegate = delegate;
    }

    public abstract String getName();
    public abstract Integer getPrice();
}
