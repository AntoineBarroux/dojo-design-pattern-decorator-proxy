package solution.model;

public interface Bank {

    double checkAmount() throws IllegalAccessException;

    double payOrWithdrawAmount(double amount) throws IllegalAccessException, IllegalArgumentException;
}
