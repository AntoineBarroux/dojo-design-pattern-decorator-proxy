package solution.model;

import fr.liksi.patterns.decorator.model.CarNameEnum;
import fr.liksi.patterns.decorator.model.CarOptionEnum;
import fr.liksi.patterns.decorator.model.Specification;

import java.util.Optional;

public class CarFactorySolution {

    public static Optional<Car> createCarFromSpecs(Specification spec) {
        if (spec.getName() != CarNameEnum.CITROENC3) {
            return Optional.empty();
        }
        return Optional.of(createCitroenC3(spec));
    }

    private static Car createCitroenC3(Specification spec) {
        Car citroen = new CitroenC3();
        if (spec.getOptions().contains(CarOptionEnum.GPS)) {
            citroen = new GPS(citroen);
        }
        if (spec.getOptions().contains(CarOptionEnum.BOARD_COMPUTER)) {
            citroen = new BoardComputer(citroen);
        }
        if (spec.getOptions().contains(CarOptionEnum.SUNROOF)) {
            citroen = new SunRoof(citroen);
        }
        return citroen;
    }
}
