package solution.controller;

import fr.liksi.patterns.decorator.controller.dto.SpecificationDTO;
import fr.liksi.patterns.decorator.mapper.SpecificationMapper;
import fr.liksi.patterns.decorator.model.Car;
import fr.liksi.patterns.decorator.model.CarFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import solution.model.CardBankProxy;

import java.util.Optional;

@RestController
public class BankControllerSolution {

    private static final Logger LOG = LoggerFactory.getLogger(BankControllerSolution.class);

    private final SpecificationMapper specificationMapper;

    public BankControllerSolution(final SpecificationMapper specificationMapper) {
        this.specificationMapper = specificationMapper;
    }

    @PutMapping("/api/banksolution")
    public HttpStatus buyCar(@RequestBody SpecificationDTO specificationDTO, @RequestParam(required = false) int pin) {
        LOG.info("Received PUT event with payload : {}", specificationDTO);
        Optional<Car> car = CarFactory.createCarFromSpecs(specificationMapper.toSpecification(specificationDTO));
        if(car.isEmpty()) {
            return HttpStatus.BAD_REQUEST;
        }
        try {
            CardBankProxy cardBankProxy = new CardBankProxy();
            LOG.info("Input pin {}", pin);
            boolean validPin = cardBankProxy.openAccessToAccount(pin);
            LOG.info("Valid pin : {}", validPin);
            LOG.info("Paying the car for an amount of : {}", car.get().getPrice());
            cardBankProxy.payOrWithdrawAmount(car.get().getPrice());
            LOG.info("Current bank amount : {}", cardBankProxy.checkAmount());
        } catch(Exception e) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return HttpStatus.OK;
    }
}
