package solution.controller;

import fr.liksi.patterns.decorator.controller.dto.SpecificationDTO;
import fr.liksi.patterns.decorator.mapper.SpecificationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import solution.model.Car;
import solution.model.CarFactorySolution;

import java.util.Optional;

@RestController
public class CarControllerSolution {

    private static final Logger LOG = LoggerFactory.getLogger(CarControllerSolution.class);

    private final SpecificationMapper specificationMapper;

    public CarControllerSolution(final SpecificationMapper specificationMapper) {
        this.specificationMapper = specificationMapper;
    }

    @PutMapping("/solution/car")
    public Optional<Car> create(@RequestBody SpecificationDTO specificationDTO) {
        LOG.info("Received PUT event with payload : {}", specificationDTO);
        return CarFactorySolution.createCarFromSpecs(specificationMapper.toSpecification(specificationDTO));
    }
}
