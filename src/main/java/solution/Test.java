package solution;

import solution.model.*;

public class Test {

    public static void main(String[] args) {
        final Car withoutOption = new CitroenC3();
        System.out.println(withoutOption);

        final Car withSunRoof = new SunRoof(withoutOption);
        System.out.println(withSunRoof);

        final Car withSunRoofAndGps = new GPS(withSunRoof);
        System.out.println(withSunRoofAndGps);

        final Car allOptions = new BoardComputer(withSunRoofAndGps);
        System.out.println(allOptions);
    }
}
