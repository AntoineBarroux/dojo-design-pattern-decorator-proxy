package fr.liksi.patterns.proxy.model;

import solution.model.Bank;

public class BankAccount implements Bank {
    private double amount;
    private int pin;

    public BankAccount(double amount, int pin) {
        this.amount = amount;
        this.pin = pin;
    }

    @Override
    public double checkAmount() {
        return amount;
    }

    @Override
    public double payOrWithdrawAmount(double amount) throws IllegalArgumentException {
        if(this.amount < amount) {
            throw new IllegalArgumentException(String.format("Trying to use %s, but account only has %s", amount, this.amount));
        }
        this.amount = this.amount - amount;
        return this.amount;
    }

    public int getPin() {
        return pin;
    }

    public void setPin(int pin) {
        this.pin = pin;
    }
}
