package fr.liksi.patterns.proxy.controller;

import fr.liksi.patterns.decorator.controller.dto.SpecificationDTO;
import fr.liksi.patterns.decorator.mapper.SpecificationMapper;
import fr.liksi.patterns.decorator.model.Car;
import fr.liksi.patterns.decorator.model.CarFactory;
import fr.liksi.patterns.proxy.model.BankAccount;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class BankController {

    private static final Logger LOG = LoggerFactory.getLogger(BankController.class);

    private final SpecificationMapper specificationMapper;

    public BankController(final SpecificationMapper specificationMapper) {
        this.specificationMapper = specificationMapper;
    }

    @PutMapping("/api/bank/car")
    public HttpStatus buyCar(@RequestBody SpecificationDTO specificationDTO, @RequestParam(required = false) int pin) {
        LOG.info("Received PUT event with payload : {}", specificationDTO);
        Optional<Car> car = CarFactory.createCarFromSpecs(specificationMapper.toSpecification(specificationDTO));
        if(car.isEmpty()) {
            return HttpStatus.BAD_REQUEST;
        }
        try {
            BankAccount bankAccount = new BankAccount(10000, 1234);
            LOG.info("Input pin {}", pin);
            if(pin == bankAccount.getPin()) {
                LOG.info("Paying the car for an amount of : {}", car.get().getPrice());
                bankAccount.payOrWithdrawAmount(car.get().getPrice());
            } else {
                throw new IllegalAccessException("Not authenticated");
            }

            LOG.info("Current bank amount : {}", bankAccount.checkAmount());
        } catch(Exception e) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return HttpStatus.OK;
    }
}
