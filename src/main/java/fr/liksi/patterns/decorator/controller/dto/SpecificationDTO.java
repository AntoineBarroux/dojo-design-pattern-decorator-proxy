package fr.liksi.patterns.decorator.controller.dto;

import fr.liksi.patterns.decorator.model.CarNameEnum;
import fr.liksi.patterns.decorator.model.CarOptionEnum;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.List;

public class SpecificationDTO {

    private final CarNameEnum name;
    private final List<CarOptionEnum> options;

    private SpecificationDTO(final CarNameEnum name, final List<CarOptionEnum> options) {
        this.name = name;
        this.options = options;
    }

    public static SpecificationDTO of(final CarNameEnum name, final List<CarOptionEnum> options) {
        return new SpecificationDTO(name, options);
    }

    public CarNameEnum getName() {
        return name;
    }

    public List<CarOptionEnum> getOptions() {
        return options;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("name", name)
                .append("options", options)
                .toString();
    }
}
