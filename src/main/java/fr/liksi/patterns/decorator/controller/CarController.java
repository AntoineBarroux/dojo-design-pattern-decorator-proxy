package fr.liksi.patterns.decorator.controller;

import fr.liksi.patterns.decorator.controller.dto.SpecificationDTO;
import fr.liksi.patterns.decorator.mapper.SpecificationMapper;
import fr.liksi.patterns.decorator.model.Car;
import fr.liksi.patterns.decorator.model.CarFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class CarController {

    private static final Logger LOG = LoggerFactory.getLogger(CarController.class);

    private final SpecificationMapper specificationMapper;

    public CarController(final SpecificationMapper specificationMapper) {
        this.specificationMapper = specificationMapper;
    }

    @PutMapping("/api/car")
    public Optional<Car> create(@RequestBody SpecificationDTO specificationDTO) {
        LOG.info("Received PUT event with payload : {}", specificationDTO);
        return CarFactory.createCarFromSpecs(specificationMapper.toSpecification(specificationDTO));
    }
}
