package fr.liksi.patterns.decorator.mapper;

import fr.liksi.patterns.decorator.controller.dto.SpecificationDTO;
import fr.liksi.patterns.decorator.model.Specification;
import org.springframework.stereotype.Component;

import static java.util.Objects.isNull;

@Component
public class SpecificationMapper {

    public Specification toSpecification(SpecificationDTO dto) {
        if (isNull(dto)) {
            return null;
        }
        final var result = new Specification();
        result.setName(dto.getName());
        result.setOptions(dto.getOptions());
        return result;
    }
}
