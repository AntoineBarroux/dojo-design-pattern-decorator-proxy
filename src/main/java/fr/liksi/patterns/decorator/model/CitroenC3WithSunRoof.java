package fr.liksi.patterns.decorator.model;

public class CitroenC3WithSunRoof extends Car {

    public CitroenC3WithSunRoof() {
        this.name = "Citroën C3 - with sunroof";
        this.price = 8000;
    }
}
