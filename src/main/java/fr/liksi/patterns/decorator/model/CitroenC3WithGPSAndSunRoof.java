package fr.liksi.patterns.decorator.model;

public class CitroenC3WithGPSAndSunRoof extends Car {

    public CitroenC3WithGPSAndSunRoof() {
        this.name = "Citroën C3 - with GPS - with sunroof";
        this.price = 9000;
    }
}
