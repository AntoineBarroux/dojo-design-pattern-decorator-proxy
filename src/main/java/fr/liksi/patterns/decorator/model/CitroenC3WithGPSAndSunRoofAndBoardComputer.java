package fr.liksi.patterns.decorator.model;

public class CitroenC3WithGPSAndSunRoofAndBoardComputer extends Car {

    public CitroenC3WithGPSAndSunRoofAndBoardComputer() {
        this.name = "Citroën C3 - with GPS - with sunroof - with board computer";
        this.price = 10500;
    }
}
