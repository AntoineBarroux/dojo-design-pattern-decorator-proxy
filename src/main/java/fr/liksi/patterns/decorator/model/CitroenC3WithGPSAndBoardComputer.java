package fr.liksi.patterns.decorator.model;

public class CitroenC3WithGPSAndBoardComputer extends Car {

    public CitroenC3WithGPSAndBoardComputer() {
        this.name = "Citroën C3 - with GPS - with board computer";
        this.price = 7500;
    }
}
