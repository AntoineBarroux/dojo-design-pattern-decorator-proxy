package fr.liksi.patterns.decorator.model;

import java.util.List;

public class Specification {

    private CarNameEnum name;
    private List<CarOptionEnum> options;

    public CarNameEnum getName() {
        return name;
    }

    public void setName(final CarNameEnum name) {
        this.name = name;
    }

    public List<CarOptionEnum> getOptions() {
        return options;
    }

    public void setOptions(final List<CarOptionEnum> options) {
        this.options = options;
    }
}
