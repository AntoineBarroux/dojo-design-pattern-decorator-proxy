package fr.liksi.patterns.decorator.model;

public enum CarOptionEnum {
    SUNROOF,
    BOARD_COMPUTER,
    GPS
}
