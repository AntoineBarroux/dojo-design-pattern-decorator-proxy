package fr.liksi.patterns.decorator.model;

import java.util.List;
import java.util.Optional;

public class CarFactory {

    public static Optional<Car> createCarFromSpecs(Specification spec) {
        if (spec.getName() != CarNameEnum.CITROENC3) {
            return Optional.empty();
        }
        return Optional.of(createCitroenC3(spec));
    }

    private static Car createCitroenC3(Specification spec) {
        final var citroen = new CitroenC3();
        if (spec.getOptions().containsAll(List.of(CarOptionEnum.GPS, CarOptionEnum.SUNROOF, CarOptionEnum.BOARD_COMPUTER))) {
            return new CitroenC3WithGPSAndSunRoofAndBoardComputer();
        } else if (spec.getOptions().containsAll(List.of(CarOptionEnum.GPS, CarOptionEnum.SUNROOF))) {
            return new CitroenC3WithGPSAndSunRoof();
        } else if (spec.getOptions().containsAll(List.of(CarOptionEnum.GPS, CarOptionEnum.BOARD_COMPUTER))) {
            return new CitroenC3WithGPSAndBoardComputer();
        } else if (spec.getOptions().containsAll(List.of(CarOptionEnum.SUNROOF, CarOptionEnum.BOARD_COMPUTER))) {
            return new CitroenC3WithSunRoofAndBoardComputer();
        } else if (spec.getOptions().contains(CarOptionEnum.GPS)) {
            return new CitroenC3WithGPS();
        } else if (spec.getOptions().contains(CarOptionEnum.SUNROOF)) {
            return new CitroenC3WithSunRoof();
        } else if (spec.getOptions().contains(CarOptionEnum.BOARD_COMPUTER)) {
            return new CitroenC3WithBoardComputer();
        } else {
            return citroen;
        }
    }
}
