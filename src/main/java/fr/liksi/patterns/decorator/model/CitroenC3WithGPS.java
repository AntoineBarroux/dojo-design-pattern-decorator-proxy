package fr.liksi.patterns.decorator.model;

public class CitroenC3WithGPS extends Car {

    public CitroenC3WithGPS() {
        this.name = "Citroën C3 - with GPS";
        this.price = 6000;
    }
}
