package fr.liksi.patterns.decorator.model;

public class CitroenC3WithBoardComputer extends Car {

    public CitroenC3WithBoardComputer() {
        this.name = "Citroën C3 - with board computer";
        this.price = 6500;
    }
}
