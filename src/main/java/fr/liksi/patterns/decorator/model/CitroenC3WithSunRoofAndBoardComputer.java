package fr.liksi.patterns.decorator.model;

public class CitroenC3WithSunRoofAndBoardComputer extends Car {

    public CitroenC3WithSunRoofAndBoardComputer() {
        this.name = "Citroën C3 - with sunroof - with board computer";
        this.price = 9500;
    }
}
