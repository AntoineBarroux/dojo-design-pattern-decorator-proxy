package fr.liksi.patterns.decorator.controller;

import fr.liksi.patterns.decorator.controller.dto.SpecificationDTO;
import fr.liksi.patterns.decorator.model.CarNameEnum;
import fr.liksi.patterns.decorator.model.CarOptionEnum;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class CarControllerTest {

    @SpyBean
    private CarController carController;

    @Test
    void should_create_basic_citroen_c3() {
        final var citroen = carController.create(SpecificationDTO.of(CarNameEnum.CITROENC3, Collections.emptyList()));
        assertThat(citroen).isPresent();
        assertThat(citroen.get()).extracting("name").isEqualTo("Citroën C3");
        assertThat(citroen.get()).extracting("price").isEqualTo(5000);
    }

    @Test
    void should_create_citroen_c3_with_gps() {
        final var citroen = carController.create(SpecificationDTO.of(CarNameEnum.CITROENC3, List.of(CarOptionEnum.GPS)));
        assertThat(citroen).isPresent();
        assertThat(citroen.get().getName())
                .contains("Citroën C3")
                .contains("- with GPS");
        assertThat(citroen.get()).extracting("price").isEqualTo(6000);
    }

    @Test
    void should_create_citroen_c3_with_board_computer() {
        final var citroen = carController.create(SpecificationDTO.of(CarNameEnum.CITROENC3, List.of(CarOptionEnum.BOARD_COMPUTER)));
        assertThat(citroen).isPresent();
        assertThat(citroen.get().getName())
                .contains("Citroën C3")
                .contains("- with board computer");
        assertThat(citroen.get()).extracting("price").isEqualTo(6500);
    }

    @Test
    void should_create_citroen_c3_with_sunroof() {
        final var citroen = carController.create(SpecificationDTO.of(CarNameEnum.CITROENC3, List.of(CarOptionEnum.SUNROOF)));
        assertThat(citroen).isPresent();
        assertThat(citroen.get().getName())
                .contains("Citroën C3")
                .contains("- with sunroof");
        assertThat(citroen.get()).extracting("price").isEqualTo(8000);
    }

    @Test
    void should_create_citroen_c3_with_gps_and_board_computer() {
        final var citroen = carController.create(SpecificationDTO.of(CarNameEnum.CITROENC3, List.of(CarOptionEnum.GPS, CarOptionEnum.BOARD_COMPUTER)));
        assertThat(citroen).isPresent();
        assertThat(citroen.get().getName())
                .contains("Citroën C3")
                .contains("- with GPS")
                .contains("- with board computer");
        assertThat(citroen.get()).extracting("price").isEqualTo(7500);
    }

    @Test
    void should_create_citroen_c3_with_gps_and_sunroof() {
        final var citroen = carController.create(SpecificationDTO.of(CarNameEnum.CITROENC3, List.of(CarOptionEnum.GPS, CarOptionEnum.SUNROOF)));
        assertThat(citroen).isPresent();
        assertThat(citroen.get().getName())
                .contains("Citroën C3")
                .contains("- with GPS")
                .contains("- with sunroof");
        assertThat(citroen.get()).extracting("price").isEqualTo(9000);
    }

    @Test
    void should_create_citroen_c3_with_board_computer_and_sunroof() {
        final var citroen = carController.create(SpecificationDTO.of(CarNameEnum.CITROENC3, List.of(CarOptionEnum.BOARD_COMPUTER, CarOptionEnum.SUNROOF)));
        assertThat(citroen).isPresent();
        assertThat(citroen.get().getName())
                .contains("Citroën C3")
                .contains("- with board computer")
                .contains("- with sunroof");
        assertThat(citroen.get()).extracting("price").isEqualTo(9500);
    }

    @Test
    void should_create_citroen_c3_all_options() {
        final var citroen = carController.create(
                SpecificationDTO.of(CarNameEnum.CITROENC3,
                List.of(CarOptionEnum.BOARD_COMPUTER, CarOptionEnum.SUNROOF, CarOptionEnum.GPS))
        );
        assertThat(citroen).isPresent();
        assertThat(citroen.get().getName())
                .contains("Citroën C3")
                .contains("- with board computer")
                .contains("- with sunroof")
                .contains("- with GPS");
        assertThat(citroen.get()).extracting("price").isEqualTo(10500);
    }
}
