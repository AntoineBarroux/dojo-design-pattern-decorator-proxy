package fr.liksi.patterns.proxy.controller;

import fr.liksi.patterns.decorator.controller.dto.SpecificationDTO;
import fr.liksi.patterns.decorator.model.CarNameEnum;
import fr.liksi.patterns.decorator.model.CarOptionEnum;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.HttpStatus;
import solution.controller.BankControllerSolution;

import java.util.Arrays;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class BankControllerTest {

    @SpyBean
    private BankController bankController;

    @SpyBean
    private BankControllerSolution bankControllerSolution;

    @Test
    void should_not_buy_basic_citroen_c3_wrong_pin() {
        final var httpStatus = bankController.buyCar(SpecificationDTO.of(CarNameEnum.CITROENC3, Collections.emptyList()), 4321);
        final var httpStatusSolution = bankControllerSolution.buyCar(SpecificationDTO.of(CarNameEnum.CITROENC3, Collections.emptyList()), 4321);
        assertThat(httpStatus).isEqualByComparingTo(HttpStatus.INTERNAL_SERVER_ERROR);
        assertThat(httpStatusSolution).isEqualByComparingTo(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Test
    void should_have_enough_to_buy_basic_citroen_c3() {
        final var httpStatus = bankController.buyCar(SpecificationDTO.of(CarNameEnum.CITROENC3, Collections.emptyList()), 1234);
        final var httpStatusSolution = bankControllerSolution.buyCar(SpecificationDTO.of(CarNameEnum.CITROENC3, Collections.emptyList()), 1234);
        assertThat(httpStatus).isEqualByComparingTo(HttpStatus.OK);
        assertThat(httpStatusSolution).isEqualByComparingTo(HttpStatus.OK);
    }

    @Test
    void should_have_enough_to_buy_custom_citroen_c3() {
        final var httpStatus = bankController.buyCar(SpecificationDTO.of(CarNameEnum.CITROENC3,  Arrays.asList(CarOptionEnum.SUNROOF, CarOptionEnum.BOARD_COMPUTER)), 1234);
        final var httpStatusSolution = bankControllerSolution.buyCar(SpecificationDTO.of(CarNameEnum.CITROENC3,  Arrays.asList(CarOptionEnum.SUNROOF, CarOptionEnum.BOARD_COMPUTER)), 1234);
        assertThat(httpStatus).isEqualByComparingTo(HttpStatus.OK);
        assertThat(httpStatusSolution).isEqualByComparingTo(HttpStatus.OK);
    }

    @Test
    void should_not_have_enough_to_buy_fully_equipped_citroen_c3() {
        final var httpStatus = bankController.buyCar(SpecificationDTO.of(CarNameEnum.CITROENC3, Arrays.asList(CarOptionEnum.GPS, CarOptionEnum.SUNROOF, CarOptionEnum.BOARD_COMPUTER)), 1234);
        final var httpStatusSolution = bankControllerSolution.buyCar(SpecificationDTO.of(CarNameEnum.CITROENC3, Arrays.asList(CarOptionEnum.GPS, CarOptionEnum.SUNROOF, CarOptionEnum.BOARD_COMPUTER)), 1234);
        assertThat(httpStatus).isEqualByComparingTo(HttpStatus.INTERNAL_SERVER_ERROR);
        assertThat(httpStatusSolution).isEqualByComparingTo(HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
